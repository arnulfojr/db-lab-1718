<html>
<head>
  <title>User Profile</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <style type="text/css">
      textarea {
          min-height: 200px;
      }
    </style>
</head>
<body>
<!-- http://getbootstrap.com/docs/3.3/components/#navbar -->
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/search">Search Babble</a>
        </li>
        <li>
          <a href="/user?username=${user.username}">My Profile</a>
        </li>
        <li>
          <a href="/logout">Logout</a>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<!-- CONTENT -->
<div class="container">
  <div class="row" style="margin-top: 50px">
    <div class="col-sm-6 col-sm-offset-3">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <a class="panel-title" href="/user?username=${user.username}">${user.name}@${user.username}</a>
        </div>
        <div class="panel-body">
          <p>${babble.text}</p>
        </div>
        <ul class="list-group">
          <li class="list-group-item">${babble.likes} Likes / ${babble.dislikes} Dislikes / ${babble.rebabbles} Rebabbles</li>
          <li class="list-group-item"><em>Created on: ${babble.createdOn}</em></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3">
      <div class="col-sm-12">
        <div class="col-xs-3" style="padding: 0">
          <form method="post" class="inline-form" action="/like">
            <input name="like" value="1" type="number" hidden>
            <input name="babble" value="${babble.id}" type="number" hidden>
            <button class="btn btn-sm btn-primary" type="submit">Like</button>
          </form>
        </div>
        <div class="col-xs-3" style="padding: 0">
          <form method="post" class="inline-form" action="/like">
            <input name="like" value="-1" type="number" hidden>
            <input name="babble" value="${babble.id}" type="number" hidden>
            <button class="btn btn-sm btn-warning" type="submit">Dislike</button>
          </form>
        </div>
        <div class="col-xs-3" style="padding: 0">
          <form method="post" class="inline-form" action="/rebabble">
            <input name="rebabble" value="1" type="number" hidden>
            <input name="babble" value="${babble.id}" type="number" hidden>
            <button class="btn btn-sm btn-default" type="submit">Rebabble</button>
          </form>
        </div>
        <div class="col-xs-3" style="padding: 0">
          <form method="post" class="inline-form" action="/delete-babble">
            <input name="_method" value="DELETE" hidden>
            <input name="babble" value="${babble.id}" type="number" hidden>
            <button class="btn btn-sm btn-danger">Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
