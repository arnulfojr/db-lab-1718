<html>
  <head>
    <title>User Profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
  <!-- http://getbootstrap.com/docs/3.3/components/#navbar -->
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li>
              <a href="/search">Search Babble</a>
            </li>
            <li>
              <a href="/user?username=${username}">My Profile</a>
            </li>
            <li>
              <a href="/logout">Logout</a>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>

    <!-- CONTENT -->
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-sm-offset-4">
          <form class="form-inline" method="post">
            <div class="form-group">
              <label class="sr-only" for="keywords">Keywords</label>
              <input id="keywords" name="keywords" type="text" class="form-control" placeholder="Keywords"/>
            </div>
            <button class="btn btn-primary" type="submit">Search</button>
          </form>
        </div>
      </div>
      <div class="row">
        <!-- http://getbootstrap.com/docs/3.3/components/#panels -->
        <#list babbles as babble>
        <div class="panel panel-default">
          <div class="panel-heading">
            <a href="/user?username=${babble.createdBy}">@${babble.createdBy}</a>
          </div>
          <div class="panel-body">
            <p>${babble.text}</p>
            <a href="/babble?id=${babble.id}">More Info...</a>
          </div>
          <ul class="list-group">
            <li class="list-group-item">${babble.likes} Likes / ${babble.dislikes} Dislikes / ${babble.rebabbles} Rebabbles</li>
            <li class="list-group-item"><em>Created on: ${babble.createdOn}</em></li>
          </ul>
        </div>
        <!-- no results -->
      </#list>
      </div>
    </div>

  </body>
</html>
