package de.unidue.inf.is.services;

import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.stores.IntegrityException;
import java.io.Closeable;
import java.io.IOException;

public class LikabilityService implements Closeable {

    private BabbleStore babbleStore;

    /**
     * Likability Type enum
     */
    public enum Likability {
        like, dislike
    }

    public LikabilityService() {
        this.babbleStore = new BabbleStore();
    }

    public void like(int babbleId, String username) {
        try {
            this.babbleStore.like(babbleId, username);
        } catch (IntegrityException e) {
            this.babbleStore.rollback();
            this.babbleStore.updateLikability(babbleId, Likability.like, username);
        } finally {
            this.babbleStore.commit();
        }
    }

    public void dislike(int babbleId, String username) {
        try {
            this.babbleStore.dislike(babbleId, username);
        } catch (IntegrityException e) {
            this.babbleStore.rollback();
            this.babbleStore.updateLikability(babbleId, Likability.dislike, username);
        } finally {
            this.babbleStore.commit();
        }
    }

    @Override
    public void close() throws IOException {
        this.babbleStore.close();
    }

}
