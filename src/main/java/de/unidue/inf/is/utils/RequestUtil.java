package de.unidue.inf.is.utils;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;

public class RequestUtil {

    /**
     * Creates a map of Key:Value from the parameters in for of application/x-www-form-urlencoded
     * param=value1&param2=value2 => param: value1, param2: value2
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
     */
    static public Map<String, String> createParameterMap(String body) {
        String[] params = body.split("&");

        Map<String, String> parameters = new HashMap<>();
        for (String param : params) {
            String[] splits = param.split("=");
            String value = splits[1];
            value = value.replace("+", " ");
            parameters.put(splits[0], value);
        }

        return parameters;
    }

    /**
     * Searches for the cookie with the name username and returns the Value
     * This is a shortcut function as it repeats a lot among the servlets
     */
    static public String getUsername(Cookie[] cookies) {
        // get the username from the cookie
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("username")) {
                return cookie.getValue();
            }
        }

        return null;
    }

}
