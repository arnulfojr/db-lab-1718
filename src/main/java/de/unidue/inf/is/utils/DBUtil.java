package de.unidue.inf.is.utils;

import com.ibm.db2.jcc.DB2BaseDataSource;
import com.ibm.db2.jcc.DB2Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public final class DBUtil {

    // TODO: Change if the database name is different
    static private String DB_NAME = "BABBLE";

    static {
        com.ibm.db2.jcc.DB2Driver driver = new DB2Driver();
        try {
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            throw new Error("Laden des Datenbanktreiber nicht möglich");
        }
    }


    public static Connection getConnection() throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("securityMechanism",
            Integer.toString(DB2BaseDataSource.CLEAR_TEXT_PASSWORD_SECURITY));

        // TODO: set the proper username and password
        properties.setProperty("user", "db2inst1");
        properties.setProperty("password", "db2inst1-pwd");

        // TODO: change to proper database
        final String url = "jdbc:db2://0.0.0.0:50000/" + DBUtil.DB_NAME
            + ":currentSchema=BABBLE;";

        return DriverManager.getConnection(url, properties);
    }


    public static boolean databaseExists() {
        boolean exists = false;

        try (Connection connection = getConnection()) {
            exists = true;
        } catch (SQLException e) {
            exists = false;
            e.printStackTrace();
        }

        return exists;
    }

}
