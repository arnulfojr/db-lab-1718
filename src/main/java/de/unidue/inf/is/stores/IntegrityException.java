package de.unidue.inf.is.stores;

public class IntegrityException extends StoreException {

    public IntegrityException(Exception e) {
        super(e);
    }
}
