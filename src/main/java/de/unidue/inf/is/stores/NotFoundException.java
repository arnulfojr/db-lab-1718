package de.unidue.inf.is.stores;

public class NotFoundException extends StoreException {

    public NotFoundException(Exception e) {
        super(e);
    }
}
