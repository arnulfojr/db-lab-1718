package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.Babble;
import de.unidue.inf.is.services.LikabilityService.Likability;
import de.unidue.inf.is.utils.DBUtil;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class BabbleStore implements Closeable {

    private Connection connection;
    private boolean complete;

    public BabbleStore() throws StoreException {
        try {
            this.connection = DBUtil.getConnection();
            this.connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Saves a Babble in the database
     */
    public void save(Babble babble) {
        try {
            String sql = "INSERT INTO BABBLE (TEXT, CREATOR) VALUES (?, ?)";
            PreparedStatement statement = this.connection.prepareStatement(sql);
            statement.setString(1, babble.getText());
            statement.setString(2, babble.getCreatedBy());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Deletes the Babble
     */
    public void delete(int babbleId) {
        try {
            String sql = "DELETE FROM BABBLE WHERE ID = ?";
            PreparedStatement statement = this.connection.prepareStatement(sql);
            statement.setInt(1, babbleId);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    public Babble get(int id) {
        try {
            String query = ""
                + "SELECT\n"
                + "  babble.ID,\n"
                + "  babble.TEXT,\n"
                + "  babble.CREATED,\n"
                + "  babble.CREATOR,\n"
                + "  likes.COUNT     AS LIKES,\n"
                + "  dislikes.COUNT  AS DISLIKES,\n"
                + "  rebabbles.COUNT AS REBABBLES\n"
                + "FROM BABBLE AS babble\n"
                + "  LEFT JOIN (\n"
                + "              SELECT\n"
                + "                likes.BABBLE,\n"
                + "                count(likes.BABBLE) AS COUNT\n"
                + "              FROM LIKESBABBLE likes\n"
                + "              WHERE likes.TYPE = 'like'\n"
                + "              GROUP BY likes.BABBLE\n"
                + "            ) AS likes ON likes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               likes.BABBLE,\n"
                + "               count(likes.BABBLE) AS COUNT\n"
                + "             FROM LIKESBABBLE likes\n"
                + "             WHERE likes.TYPE = 'dislike'\n"
                + "             GROUP BY likes.BABBLE) AS dislikes ON dislikes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               rebabbles.BABBLE,\n"
                + "               count(rebabbles.BABBLE) AS COUNT\n"
                + "             FROM REBABBLE rebabbles\n"
                + "             GROUP BY rebabbles.BABBLE) AS rebabbles ON rebabbles.BABBLE = babble.ID\n"
                + "WHERE babble.ID = ?\n";
            PreparedStatement statement = this.connection.prepareStatement(query);
            statement.setInt(1, id);

            ResultSet results = statement.executeQuery();

            if (!results.next()) {
                throw new NotFoundException(null);
            }

            Babble babble = new Babble();
            // populate the Babble from the result row
            babble.setId(results.getInt("ID"));
            babble.setText(results.getString("TEXT"));
            babble.setCreatedOn(results.getTimestamp("CREATED"));
            babble.setCreatedBy(results.getString("CREATOR"));
            babble.setLikes(results.getInt("LIKES"));
            babble.setDislikes(results.getInt("DISLIKES"));
            babble.setRebabbles(results.getInt("REBABBLES"));

            return babble;
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * returns a list of Babbles created by the passed username
     * If user has no Babbles then returns an empty list
     */
    public List<Babble> getAllFrom(String username) {
        try {
            String query = ""
                + "SELECT\n"
                + "  babble.ID,\n"
                + "  babble.TEXT,\n"
                + "  babble.CREATED,\n"
                + "  babble.CREATOR,\n"
                + "  likes.COUNT     AS LIKES,\n"
                + "  dislikes.COUNT  AS DISLIKES,\n"
                + "  rebabbles.COUNT AS REBABBLES\n"
                + "FROM BABBLE AS babble\n"
                + "  LEFT JOIN (\n"
                + "              SELECT\n"
                + "                likes.BABBLE,\n"
                + "                count(likes.BABBLE) AS COUNT\n"
                + "              FROM LIKESBABBLE likes\n"
                + "              WHERE likes.TYPE = 'like'\n"
                + "              GROUP BY likes.BABBLE\n"
                + "            ) AS likes ON likes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               likes.BABBLE,\n"
                + "               count(likes.BABBLE) AS COUNT\n"
                + "             FROM LIKESBABBLE likes\n"
                + "             WHERE likes.TYPE = 'dislike'\n"
                + "             GROUP BY likes.BABBLE) AS dislikes ON dislikes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               rebabbles.BABBLE,\n"
                + "               count(rebabbles.BABBLE) AS COUNT\n"
                + "             FROM REBABBLE rebabbles\n"
                + "             GROUP BY rebabbles.BABBLE) AS rebabbles ON rebabbles.BABBLE = babble.ID\n"
                + "WHERE babble.CREATOR = ?\n"
                + "ORDER BY babble.CREATED DESC";
            PreparedStatement statement = this.connection.prepareStatement(query);
            // set the creator
            statement.setString(1, username);
            ResultSet results = statement.executeQuery();

            List<Babble> babbles = new LinkedList<>();

            // Loop over the results
            while (results.next()) {
                Babble babble = new Babble();
                // populate the Babble from the result row
                babble.setId(results.getInt("ID"));
                babble.setText(results.getString("TEXT"));
                babble.setCreatedOn(results.getTimestamp("CREATED"));
                babble.setCreatedBy(results.getString("CREATOR"));
                babble.setLikes(results.getInt("LIKES"));
                babble.setDislikes(results.getInt("DISLIKES"));
                babble.setRebabbles(results.getInt("REBABBLES"));

                babbles.add(babble);
            }

            results.close();

            return babbles;
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Returns a list of Babbles where the keyword passed matches in the body
     * If no matches then returns an empty list
     *
     * Same code as above, just the WHERE statement changes
     */
    public List<Babble> searchFor(String keyword) {
        try {
            String query = ""
                + "SELECT\n"
                + "  babble.ID,\n"
                + "  babble.TEXT,\n"
                + "  babble.CREATED,\n"
                + "  babble.CREATOR,\n"
                + "  likes.COUNT     AS LIKES,\n"
                + "  dislikes.COUNT  AS DISLIKES,\n"
                + "  rebabbles.COUNT AS REBABBLES\n"
                + "FROM BABBLE AS babble\n"
                + "  LEFT JOIN (\n"
                + "              SELECT\n"
                + "                likes.BABBLE,\n"
                + "                count(likes.BABBLE) AS COUNT\n"
                + "              FROM LIKESBABBLE likes\n"
                + "              WHERE likes.TYPE = 'like'\n"
                + "              GROUP BY likes.BABBLE\n"
                + "            ) AS likes ON likes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               likes.BABBLE,\n"
                + "               count(likes.BABBLE) AS COUNT\n"
                + "             FROM LIKESBABBLE likes\n"
                + "             WHERE likes.TYPE = 'dislike'\n"
                + "             GROUP BY likes.BABBLE) AS dislikes ON dislikes.BABBLE = babble.ID\n"
                + "  LEFT JOIN (SELECT\n"
                + "               rebabbles.BABBLE,\n"
                + "               count(rebabbles.BABBLE) AS COUNT\n"
                + "             FROM REBABBLE rebabbles\n"
                + "             GROUP BY rebabbles.BABBLE) AS rebabbles ON rebabbles.BABBLE = babble.ID\n"
                + "WHERE LOWER(babble.TEXT) LIKE LOWER(?)\n"
                + "ORDER BY babble.CREATED DESC";
            PreparedStatement statement = this.connection.prepareStatement(query);
            statement.setString(1, "%" + keyword + "%");
            ResultSet results = statement.executeQuery();

            List<Babble> babbles = new LinkedList<>();

            while (results.next()) {
                Babble babble = new Babble();
                babble.setId(results.getInt("ID"));
                babble.setText(results.getString("TEXT"));
                babble.setCreatedOn(results.getTimestamp("CREATED"));
                babble.setCreatedBy(results.getString("CREATOR"));
                babble.setLikes(results.getInt("LIKES"));
                babble.setDislikes(results.getInt("DISLIKES"));
                babble.setRebabbles(results.getInt("REBABBLES"));
                babbles.add(babble);
            }

            results.close();

            return babbles;
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Adds an entry of the user and babble of type like
     */
    public void like(int babbleId, String username) {
        try {
            this.likeBabble(babbleId, username, Likability.like);
        } catch (StoreException e) {
            throw new IntegrityException(e);
        }
    }

    /**
     * Adds an entry of the user and babble of type dislike
     */
    public void dislike(int babbleId, String username) {
        try {
            this.likeBabble(babbleId, username, Likability.dislike);
        } catch (StoreException e) {
            throw new IntegrityException(e);
        }
    }

    private void likeBabble(int babbleId, String username, Likability type) {
        try {
            String sql = "INSERT INTO LIKESBABBLE (USERNAME, BABBLE, TYPE) VALUES (?, ?, ?)";
            PreparedStatement statement = this.connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setInt(2, babbleId);
            statement.setString(3, type.toString());

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    public void updateLikability(int babbleId, Likability type, String username) {
        try {
            String sql = "UPDATE LIKESBABBLE SET TYPE = ? WHERE USERNAME = ? AND BABBLE = ?";
            PreparedStatement statement = this.connection.prepareStatement(sql);
            statement.setString(1, type.toString());
            statement.setString(2, username);
            statement.setInt(3, babbleId);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Adds an entry in the REBABBLE table
     */
    public void rebabble(int babbleId, String username) {
        try {
            String sql = "INSERT INTO REBABBLE (USERNAME, BABBLE) VALUES (?, ?)";
            PreparedStatement statement = this.connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setInt(2, babbleId);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    public void commit() {
        try {
            this.connection.commit();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    public void rollback() {
        try {
            this.connection.rollback();
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    public void complete() {
        this.complete = true;
    }

    @Override
    public void close() throws IOException {
        if (this.connection != null) {
            try {
                if (this.complete) {
                    this.connection.commit();
                } else {
                    this.connection.rollback();
                }
            } catch (SQLException e) {
                throw new StoreException(e);
            } finally {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    throw new StoreException(e);
                }
            }
        }
    }
}
