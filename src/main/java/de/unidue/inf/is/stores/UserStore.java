package de.unidue.inf.is.stores;

import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.utils.DBUtil;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public final class UserStore implements Closeable {

    private Connection connection;
    private boolean complete;


    public UserStore() throws StoreException {
        try {
            this.connection = DBUtil.getConnection();
            // sets the connection's autocommit to false
            this.connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }


    public void save(User user) throws StoreException {
        try {
            PreparedStatement preparedStatement = this.connection
                .prepareStatement(
                    "INSERT INTO BABBLEUSER (username, name, status, foto) values (?, ?, ?, ?)");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getStatus());
            preparedStatement.setString(4, user.getPhoto());

            preparedStatement.executeUpdate();
            this.connection.commit(); // TODO: improve, maybe throught the closeable interface?
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }

    /**
     * Gets a user from its Username
     */
    public User get(String username) throws StoreException {

        final String photoUrl = "https://png.icons8.com/color/260/person-male.png";

        try {
            PreparedStatement statement = this.connection
                .prepareStatement(
                    "SELECT username, name, status, NVL(foto, ?) AS photo FROM BABBLEUSER WHERE username = ?");
            statement.setString(1, photoUrl);
            statement.setString(2, username);
            ResultSet result = statement.executeQuery();

            if (!result.next()) {
                // means that there were no results, throw exception
                throw new NotFoundException(null);
            }

            User user = new User();
            // populate the user from the result set
            user.setUsername(result.getString("username"));
            user.setName(result.getString("name"));
            user.setStatus(result.getString("status"));
            user.setPhoto(result.getString("photo"));

            // close the ResultSet
            result.close();

            return user;
        } catch (SQLException e) {
            throw new StoreException(e);
        }
    }


    public void complete() {
        this.complete = true;
    }


    @Override
    public void close() throws IOException {
        if (this.connection != null) {
            try {
                if (this.complete) {
                    this.connection.commit();
                } else {
                    this.connection.rollback();
                }
            } catch (SQLException e) {
                throw new StoreException(e);
            } finally {
                try {
                    this.connection.close();
                } catch (SQLException e) {
                    throw new StoreException(e);
                }
            }
        }
    }
}
