package de.unidue.inf.is.servlets;

import de.unidue.inf.is.domain.Babble;
import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.stores.NotFoundException;
import de.unidue.inf.is.stores.UserStore;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Einfaches Beispiel, das die Verwendung des {@link UserStore}s zeigt.
 */
public final class UserServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        String username = request.getParameter("username");

        UserStore userStore = new UserStore();
        BabbleStore babbleStore = new BabbleStore();

        User user;
        try {
            user = userStore.get(username);
        } catch (NotFoundException e) {
            response.sendError(404, "User with username " + username + " was not found");
            return;
        }
        request.setAttribute("user", user);

        List<Babble> babbles = babbleStore.getAllFrom(user.getUsername());
        request.setAttribute("babbles", babbles);


        request.getRequestDispatcher("user_profile.ftl")
            .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        // TODO: create a user??
    }
}
