package de.unidue.inf.is.servlets;

import de.unidue.inf.is.stores.NotFoundException;
import de.unidue.inf.is.stores.UserStore;
import de.unidue.inf.is.utils.RequestUtil;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

    /* Render the login view, if user is logged in then redirect to to profile view */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        String username = RequestUtil.getUsername(req.getCookies());

        // if there's a username cookie then the username is not null
        boolean isLoggedIn = null != username;

        if (isLoggedIn) {
            // redirect to profile
            resp.sendRedirect("/user?username=" + username);
        } else {
            // render the view to login
            req.getRequestDispatcher("login_view.ftl").forward(req, resp);
        }
    }

    /**
     * Check if user exists in the database, if it does add a Cookie with its username
     * for future tracking.
     *
     * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        UserStore userStore = new UserStore();

        // get the request body
        String requestBody = req.getReader().lines()
            .collect(Collectors.joining(System.lineSeparator()));

        // Get the request parameters in a Map way...
        Map<String, String> parameters = RequestUtil.createParameterMap(requestBody);
        String username = parameters.get("username");

        try {
            userStore.get(username);  // if there's no user found it throws the exception
        } catch (NotFoundException e) {
            resp.sendError(404);
            return;
        }

        // there's a user, set the cookie with its username
        Cookie cookie = new Cookie("username", username);
        cookie.setMaxAge(-1);  // session long cookie
        resp.addCookie(cookie);

        // redirect user to user profile
        resp.sendRedirect("/user?username=" + username);
    }
}
