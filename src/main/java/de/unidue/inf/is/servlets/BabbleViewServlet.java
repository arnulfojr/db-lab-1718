package de.unidue.inf.is.servlets;

import de.unidue.inf.is.domain.Babble;
import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.stores.NotFoundException;
import de.unidue.inf.is.stores.UserStore;
import de.unidue.inf.is.utils.RequestUtil;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class BabbleViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        /*
        Do user validation
        */
        String username = RequestUtil.getUsername(request.getCookies());

        if (null == username) {
            response.sendRedirect("/login");
            return;
        }

        UserStore userStore = new UserStore();

        User user;
        try {
            user = userStore.get(username);
        } catch (NotFoundException e) {
            // there was a username in the cookie, logout
            response.sendRedirect("/logout");
            return;
        }

        request.setAttribute("user", user);

        /*
        Now get the babble
         */

        String babbleParameter = request.getParameter("id");
        int babbleId = Integer.parseInt(babbleParameter);

        BabbleStore store = new BabbleStore();

        Babble babble;
        try {
            babble = store.get(babbleId);
        } catch (NotFoundException e) {
            response.sendError(404, "Babble was not found");
            return;
        }

        request.setAttribute("babble", babble);

        request.getRequestDispatcher("babble_view.ftl")
            .forward(request, response);
    }
}
