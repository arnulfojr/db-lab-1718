package de.unidue.inf.is.servlets;

import de.unidue.inf.is.domain.Babble;
import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.utils.RequestUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        // set an empty list to babbles (there's no Babbles to render)
        request.setAttribute("babbles", new ArrayList<Babble>());
        String username = RequestUtil.getUsername(request.getCookies());

        if (null == username) {
            // there's no user logged in
            response.sendRedirect("/login");
            return;
        }

        // set the username as a request attribute
        // this makes the variable present in the template to render
        request.setAttribute("username", username);

        request.getRequestDispatcher("search_view.ftl")
            .forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        // a user has to be logged in to use the search
        String username = RequestUtil.getUsername(request.getCookies());
        if (null == username) {
            response.sendRedirect("/login");
            return;
        }

        BabbleStore babbleStore = new BabbleStore();

        String requestBody = request.getReader().lines()
            .collect(Collectors.joining(System.lineSeparator()));

        // Get a map of the parameters in the body
        Map<String, String> parameters = RequestUtil.createParameterMap(requestBody);

        // get the keyword from the parameters
        String keyword = parameters.get("keywords");

        // search babbles containing the keyword
        List<Babble> babbles = babbleStore.searchFor(keyword);

        // pas the attributes to the template
        request.setAttribute("babbles", babbles);
        request.setAttribute("username", username);

        request.getRequestDispatcher("search_view.ftl").forward(request, response);
    }
}
