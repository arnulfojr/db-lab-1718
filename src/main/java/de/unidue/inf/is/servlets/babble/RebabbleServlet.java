package de.unidue.inf.is.servlets.babble;

import de.unidue.inf.is.domain.User;
import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.stores.NotFoundException;
import de.unidue.inf.is.stores.StoreException;
import de.unidue.inf.is.stores.UserStore;
import de.unidue.inf.is.utils.RequestUtil;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RebabbleServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        /*
        Do user validation
        */
        String username = RequestUtil.getUsername(request.getCookies());

        if (null == username) {
            response.sendRedirect("/login");
            return;
        }

        UserStore userStore = new UserStore();

        User user;
        try {
            user = userStore.get(username);
        } catch (NotFoundException e) {
            // there was a username in the cookie, logout
            response.sendRedirect("/logout");
            return;
        }

        request.setAttribute("user", user);

        /*
        Handle form update
        */
        String requestBody = request.getReader().lines()
            .collect(Collectors.joining(System.lineSeparator()));
        Map<String, String> parameters = RequestUtil.createParameterMap(requestBody);

        int babbleId = Integer.parseInt(parameters.get("babble"));
        BabbleStore babbleStore = new BabbleStore();

        // we do a rebabble
        try {
            babbleStore.rebabble(babbleId, username);
        } catch (StoreException e) {
            // do nothing... the user already rebabbble once...
            babbleStore.rollback();
        } finally {
            babbleStore.complete();
            babbleStore.close();
        }

        response.sendRedirect("/babble?id=" + babbleId);
    }
}
