package de.unidue.inf.is.servlets;

import de.unidue.inf.is.domain.Babble;
import de.unidue.inf.is.stores.BabbleStore;
import de.unidue.inf.is.utils.RequestUtil;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * This handles the Create-Babble View and creation
 */
public final class CreateBabbleServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    /* Simply returns the view to create a template */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String username = RequestUtil.getUsername(request.getCookies());

        if (null == username) {
            response.sendRedirect("/login");
            return;
        }

        request.setAttribute("username", username);
        request.getRequestDispatcher("create_babble.ftl").forward(request, response);
    }


    /**
     *  Post handler to create a Babble
     *  @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String username = RequestUtil.getUsername(request.getCookies());

        if (null == username) {
            // no username cookie, means there's no user, redirect to login
            response.sendRedirect("/login");
            return;
        }

        // read the request body (comes in the form of application/x-www-form-urlencoded)
        String requestBody = request.getReader().lines()
            .collect(Collectors.joining(System.lineSeparator()));

        // create the map function
        Map<String, String> parameters = RequestUtil.createParameterMap(requestBody);

        Babble babble = new Babble();
        babble.setText(parameters.get("text"));
        babble.setCreatedBy(username);

        BabbleStore store = new BabbleStore();
        store.save(babble);  // save the babble to the database

        store.complete();
        store.close();

        // redirect to the profile view
        response.sendRedirect("/user?username=" + username);
    }
}
