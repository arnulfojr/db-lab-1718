package de.unidue.inf.is.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        // remove the cookie by resetting it
        Cookie cookie = new Cookie("username", null);
        cookie.setMaxAge(0);
        resp.addCookie(cookie);

        // redirect to login
        resp.sendRedirect("/login");
    }
}
