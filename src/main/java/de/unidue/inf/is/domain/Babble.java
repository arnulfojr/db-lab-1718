package de.unidue.inf.is.domain;

import java.sql.Timestamp;

public class Babble {

    private int id;

    private String text;

    private Timestamp createdOn;

    private String createdBy;

    private int likes;

    private int dislikes;

    private int rebabbles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public int getRebabbles() {
        return rebabbles;
    }

    public void setRebabbles(int rebabbles) {
        this.rebabbles = rebabbles;
    }
}
